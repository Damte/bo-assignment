package ee.backoffice.assignment.repository;

import ee.backoffice.assignment.domain.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List<Customer> findAllByFirstName(String firstName);
    List<Customer> findByLastName(String lastName);
    Customer findByPersonalId(String personalId);
}
