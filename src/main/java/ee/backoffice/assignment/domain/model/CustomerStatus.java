package ee.backoffice.assignment.domain.model;

/**
 * @author Sander Kadajane
 * @since 31.01.2019
 */
public enum CustomerStatus {
    OPENED, BLOCKED, ACTIVE, CLOSED
}
