package ee.backoffice.assignment.exception;

public class DestinationAccountCantBeSameAsOriginAccount extends Throwable {
    public DestinationAccountCantBeSameAsOriginAccount(String s) {
        super(s);
    }

}
