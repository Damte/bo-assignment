package ee.backoffice.assignment.exception;

public class AboveAmountLimit extends Throwable {

    public AboveAmountLimit(String message) {
        super(message);
    }
}
