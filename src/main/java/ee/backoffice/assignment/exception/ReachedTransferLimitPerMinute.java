package ee.backoffice.assignment.exception;

public class ReachedTransferLimitPerMinute extends Throwable {

    public ReachedTransferLimitPerMinute(String message) {
        super(message);
    }
}
