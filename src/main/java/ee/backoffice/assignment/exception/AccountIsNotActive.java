package ee.backoffice.assignment.exception;

public class AccountIsNotActive extends Throwable {
    public AccountIsNotActive(String s) {
        super(s);
    }

}
