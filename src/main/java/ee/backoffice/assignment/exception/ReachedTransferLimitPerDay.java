package ee.backoffice.assignment.exception;

public class ReachedTransferLimitPerDay extends Throwable {

    public ReachedTransferLimitPerDay(String message) {
        super(message);
    }
}
