package ee.backoffice.assignment.exception;

public class AccountNotFound extends Throwable {
    public AccountNotFound(String s) {
        super(s);
    }

}
