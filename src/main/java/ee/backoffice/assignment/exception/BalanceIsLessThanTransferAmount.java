package ee.backoffice.assignment.exception;

public class BalanceIsLessThanTransferAmount extends Throwable {
    public BalanceIsLessThanTransferAmount(String s) {
        super(s);
    }

}
