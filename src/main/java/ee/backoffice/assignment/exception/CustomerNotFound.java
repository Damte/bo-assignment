package ee.backoffice.assignment.exception;

public class CustomerNotFound extends Throwable {
    public CustomerNotFound(String s) {
        super(s);
    }

}
