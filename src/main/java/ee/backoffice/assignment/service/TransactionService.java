package ee.backoffice.assignment.service;

import ee.backoffice.assignment.domain.model.Account;
import ee.backoffice.assignment.dto.TransactionDTO;
import ee.backoffice.assignment.dto.TransactionDTOListTransactionsByAccount;
import ee.backoffice.assignment.exception.*;

import java.util.List;

public interface TransactionService {
    List<Account> doTransaction(TransactionDTO transaction) throws AccountIsNotActive, BalanceIsLessThanTransferAmount, AboveAmountLimit, ReachedTransferLimitPerDay, ReachedTransferLimitPerMinute, AccountNotFound;

    List<TransactionDTOListTransactionsByAccount> findTransactionsByAccount(Long accountId);
}
