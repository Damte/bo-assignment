package ee.backoffice.assignment.service;

import ee.backoffice.assignment.domain.model.Customer;
import ee.backoffice.assignment.exception.CustomerNotFound;

import java.util.List;

public interface CustomerService {
    Customer saveCustomer(Customer customer);

    List<Customer> findAllCustomers();

    List<Customer> findCustomersByFirstName(String firstName);

    Customer findCustomer(Long customerId) throws CustomerNotFound;

    Customer findCustomerByPersonalId(String personalId);

    Customer setStatus(Long customerId, String status);
}
